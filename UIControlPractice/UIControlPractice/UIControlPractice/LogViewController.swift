//
//  LogViewController.swift
//  UIControlPractice
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class LogViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
   
    @IBOutlet weak var userName: UITextField!
    
    @IBAction func userNameEndEdit(sender: UITextField) {
        
        sender.resignFirstResponder()
    }
    
    @IBOutlet weak var password: UITextField!
    
    @IBAction func passwordEndEdit(sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func logInButton(sender: UIButton) {
        
        if (userName.text == "sundy" && password.text == "sundy") {
            print("log in succeed")
        }
        else {
            print("invalid username or password")
        }
        
    }


    @IBAction func resetButton(sender: UIButton) {
        
        userName.text = ""
        password.text = ""
    }
    
    // slider practice
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var slider: UISlider!
    

    @IBAction func slider(sender: UISlider) {
        
        image.alpha = CGFloat (sender.value)
       
    }
    
    //switch practice
    
    @IBAction func `switch`(sender: UISwitch) {
        
        if sender.on {
            
            self.view.backgroundColor = UIColor.whiteColor()
        }
        else {
            self.view.backgroundColor = UIColor.blackColor()
        }
        
    }
    
    
    
    
}
