//
//  SegmentControlClass.swift
//  UIControlPractice
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class SegmentController: UIViewController {

    
    @IBAction func segmentControl(sender: UISegmentedControl) {
        print(self)
        switch sender.selectedSegmentIndex {
        case 0:
            self.view.backgroundColor = UIColor.blueColor()
        case 1:
            self.view.backgroundColor = UIColor.whiteColor()
        case 2:
            self.view.backgroundColor = UIColor.greenColor()
        default: self.view.backgroundColor = UIColor.blackColor()
        }
    }
    
    @IBOutlet weak var segmentOutlet: UISegmentedControl!
    
    
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBAction func textField1(sender: UITextField) {
        
        resignFirstResponder()
    }
    
    @IBAction func addButton(sender: UIButton) {
        segmentOutlet.insertSegmentWithTitle(textField.text, atIndex: segmentOutlet.numberOfSegments, animated: true)
        
        
    }
    
    

}
