//
//  UISegmentViewController.swift
//  UIControlPractice
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class UISegmentViewController: UIViewController{

    //UIProgressView Practice
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressView.progress = 0
        NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "changeProgress:", userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    
    func changeProgress(sender: NSTimer){
        progressView.progress += 0.1
        if  progressView.progress == 1 {
            sender.invalidate()
        }
    }

    @IBAction func segment(sender: UISegmentedControl) {
        print(self)
        switch sender.selectedSegmentIndex {
        case 0:
            self.view.backgroundColor = UIColor.blueColor()
        case 1:
            self.view.backgroundColor = UIColor.whiteColor()
        case 2:
            self.view.backgroundColor = UIColor.greenColor()
        default: self.view.backgroundColor = UIColor.blackColor()
        }

    }
    
    @IBOutlet weak var textField1: UITextField!
    
    @IBOutlet weak var segmentContent: UISegmentedControl!
    
    @IBAction func buttonAdd(sender: UIButton) {
        
        segmentContent.insertSegmentWithTitle(textField1.text, atIndex: segmentContent.numberOfSegments, animated: true)
        
    }
    
    @IBAction func buttonRemove(sender: UIButton) {
        segmentContent.removeSegmentAtIndex(2, animated: true)
    }
    
   
    @IBAction func textField2(sender: UITextField) {
        print("did end on exit")
    }
    
    
    
    

}
