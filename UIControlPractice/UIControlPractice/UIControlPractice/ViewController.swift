//
//  ViewController.swift
//  UIControlPractice
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var aIV: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        aIV.startAnimating()
  //      aIV.hidesWhenStopped = true
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    //button events practice
    
    @IBAction func touchDown(sender: UIButton) {
        print("touch down")
        aIV.stopAnimating()
    }
    
    
    @IBAction func touchDownRepeat(sender: UIButton) {
        print("touch down repeat")
    }
    
    @IBAction func dragEnter(sender: UIButton) {
        print("drag enter")
    }
    
    @IBAction func dragExit(sender: UIButton) {
        print("drag exit")
    }
    
    @IBAction func dragInside(sender: UIButton) {
        print("drag inside")
    }
    
    @IBAction func dragOutside(sender: UIButton) {
        print("drag outside")
    }
    
    @IBAction func touchUpInside(sender: UIButton) {
        
        print("touch up inside")
    }
    
    @IBAction func touchUpOutside(sender: UIButton) {
        print("touch up outside")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

