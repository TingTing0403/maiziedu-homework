//
//  ViewController.swift
//  GesturePractice
//
//  Created by liu ting on 10/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myView: UIView!
    
    @IBAction func tapped(sender: UITapGestureRecognizer) {
        
        myView.backgroundColor = UIColor.magentaColor()
        
    }
    
    @IBAction func pinched(sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        myView.transform = CGAffineTransformMakeScale(sender.scale, sender.scale)
        
    }
    
    @IBAction func rotation(sender: UIRotationGestureRecognizer) {
        
        print(sender.rotation)
        myView.transform = CGAffineTransformMakeRotation(sender.rotation)
    }
    
    @IBAction func swipeEnd(sender: UISwipeGestureRecognizer) {
        print("swipe")
      
        myView.transform = CGAffineTransformMakeScale(200, 0)
        
    }
    
    @IBAction func pan(sender: UIPanGestureRecognizer) {
        print("pan")
        
        myView.transform = CGAffineTransformMakeTranslation(sender.translationInView(self.view).x, sender.translationInView(self.view).y)
       
    }
    
    @IBAction func longPress(sender: UILongPressGestureRecognizer) {
        print("longPress")
        if sender.state == UIGestureRecognizerState.Began {
            print("begin")
        }
        if sender.state == UIGestureRecognizerState.Ended{
            print("Ended")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   


}

