//
//  ViewController.swift
//  gestureProject
//
//  Created by liu ting on 10/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var myLabel = UILabel()
    var circle = UIImageView()
    var filledCircle = UIImageView()
    @IBAction func tapped(sender: UITapGestureRecognizer) {
        print("tapped")
        clearSubviews("Tapped")
 //       myLabel.text = "Tapped"
  //      self.view.addSubview(myLabel)
        pressAnimation(sender.locationInView(self.view))
       
    }
    
    @IBAction func pinched(sender: UIPinchGestureRecognizer) {
        print("pinched")
        clearSubviews("pinched")
 //       myLabel.text = "pinched"
        
        self.view.transform = CGAffineTransformMakeScale(sender.scale, sender.scale)
        if sender.state == UIGestureRecognizerState.Ended {
            self.view.transform = CGAffineTransformMakeScale(1, 1)
        }
    }
    
    @IBAction func rotation(sender: UIRotationGestureRecognizer) {
        print("rotating")
        clearSubviews("rotating")
        myLabel.transform = CGAffineTransformMakeRotation(sender.rotation)
        if sender.state == UIGestureRecognizerState.Ended {
            myLabel.transform = CGAffineTransformMakeRotation(0)
        }
    }
    
    @IBAction func swipeRight(sender: UISwipeGestureRecognizer) {
        print("swipeRight")
    
        swipeAnimation(sender.locationInView(self.view), dX: 100, dY: 0)
        
    }
    
    @IBAction func swipeLeft(sender: UISwipeGestureRecognizer) {
        print("swipeLeft")
        swipeAnimation(sender.locationInView(self.view), dX: -100, dY: 0)
    }
    
    @IBAction func swipeDown(sender: UISwipeGestureRecognizer) {
        print("swipeDown")
        swipeAnimation(sender.locationInView(self.view), dX: 0, dY: 100)
    }
    
    @IBAction func swipeUp(sender: UISwipeGestureRecognizer) {
        print("swipeUp")
        swipeAnimation(sender.locationInView(self.view), dX: 0, dY: -100)
    }
    
    
    
    @IBAction func pan(sender: UIPanGestureRecognizer) {
        print("pan")
        clearSubviews("pan")
   //     myLabel.text = "pan"
        myLabel.transform = CGAffineTransformMakeTranslation(sender.translationInView(self.view).x, sender.translationInView(self.view).y)
        
        
    }
    
    @IBAction func longPress(sender: UILongPressGestureRecognizer) {
        print("longPress")
        clearSubviews("longPress")
        if sender.state == UIGestureRecognizerState.Began {
            print("begin")
            pressAnimation(sender.locationInView(self.view))
   //         myLabel.text = "long press"
        }
        if sender.state == UIGestureRecognizerState.Ended{
            print("Ended")
  //          myLabel.text = ""
            circle.removeFromSuperview()
            filledCircle.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        myLabel.text = "label"
        
        myLabel.frame.size = CGSize(width: 100, height: 40)
        myLabel.center = self.view.center
        myLabel.backgroundColor = UIColor.greenColor()
        myLabel.textAlignment = NSTextAlignment.Center
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pressAnimation(circleCenter:CGPoint){
        print("func")
        circle = UIImageView(image: UIImage(named: "circle"))
        filledCircle = UIImageView(image: UIImage(named: "filledCircle"))
     
        circle.frame.size = CGSize(width: 20, height: 20)
        
        circle.contentMode = UIViewContentMode.ScaleToFill
        filledCircle.contentMode = UIViewContentMode.ScaleToFill
        circle.layer.opacity = 1
        circle.center = circleCenter
    
       
        filledCircle.frame = circle.frame
        self.view.addSubview(circle)
        self.view.addSubview(filledCircle)
        UIView.animateWithDuration(2, delay: 0, options: [UIViewAnimationOptions.CurveEaseInOut, UIViewAnimationOptions.Repeat], animations: {()->Void in
            print("ANIMATION1")
            self.circle.transform = CGAffineTransformMakeScale(2, 2)
            self.circle.layer.opacity = 0
            }, completion:nil)
        
    }
    
    func clearSubviews(labelText:String){
        if self.view.subviews.isEmpty == false{
        for subview in self.view.subviews {
            subview.removeFromSuperview()
           
        }
        }
        
         myLabel.text = labelText
        myLabel.layer.opacity = 1
        self.view.addSubview(myLabel)
    }
    
    func swipeAnimation(center:CGPoint, dX:CGFloat, dY:CGFloat){
        if self.view.subviews.isEmpty == false{
        for subview in self.view.subviews {
            subview.removeFromSuperview()
        }
        }
        myLabel.text = "swipe"
        myLabel.center = center
        
        self.view.addSubview(myLabel)
        myLabel.layer.opacity = 1
        UIView.animateWithDuration(2, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {()->Void in
            self.myLabel.transform = CGAffineTransformMakeTranslation(dX, dY)
            self.myLabel.layer.opacity = 0
            
            }, completion: nil)
    }
    
    
    


}

