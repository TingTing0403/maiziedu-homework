//
//  ViewController.swift
//  PrimeNumberChecker
//
//  Created by liu ting on 8/25/15.
//  Copyright (c) 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    var calculationStart = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    

    //input the scale of numbers
    @IBOutlet weak var numStart: UITextField!
    @IBAction func inputEnd(sender: UITextField) {
        sender.resignFirstResponder()
        print(numStart.text)
    }
    @IBOutlet weak var numEnd: UITextField!
    
    @IBAction func inputEnd2(sender: UITextField) {
        sender.resignFirstResponder()
        print(numEnd.text)
    }
    
//press start button, start calculation
    @IBAction func StartCalculation(sender: UIButton) {
        calculationStart = true
        mainCalculation()
        print("user pressed the button")
    }
    
//output
    @IBOutlet weak var PrimeNums: UILabel!
    
    @IBOutlet weak var amountOfPrimeNums: UILabel!
//check prime number

    func primeNumChecker(m:Int)->Bool{
        let n = Int(sqrt(Double(m)))
        if m == 1 {return false}
        else {
            for var i=2; i<=n; i++ {
            if m%i == 0{
                return false
            }
            }
            return true
        }
    }
    
  
    
// check prime number in the scale
    func mainCalculation (){
        var answer = [Int]()
        let a = Int(numStart.text!)!//convert string in Text to Int
        
        let b = Int(numEnd.text!)!
        
        if a >= b {
            PrimeNums.text = "please ensure b > a!"
        }
        else {
        if calculationStart == true {
            calculationStart = false
            for i in a...b {
                
                if primeNumChecker(i) == true {
                    answer.append(i)
                }
                
            }
            if answer.isEmpty == false {
                amountOfPrimeNums.text = ("\(answer.count) Prime Numbers")
                PrimeNums.text=answer.description
                print(answer.description)
                
            }
            else
            {
                amountOfPrimeNums.text="No Prime Number"
                PrimeNums.text = "..."
                print("No Prime Number")
                
            }
        }
        }
    }

    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

