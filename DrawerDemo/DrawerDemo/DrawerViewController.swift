//
//  DrawerViewController.swift
//  DrawerDemo
//
//  Created by liu ting on 10/3/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class DrawerViewController: UIViewController//, UITableViewDataSource, UITableViewDelegate 
{
    
    var myReuseIdentifier:String! = "leftTableView"
    
    @IBOutlet weak var mainTableView: UITableView!
    
  
    @IBOutlet weak var mainTableViewCell: UITableView!

//    var leftTableView = UITableView(frame: CGRect(x: 0, y: 64, width: 400, height: 600), style: UITableViewStyle.Plain)
//    var leftTableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "leftTableView")
    var mainLayer:CALayer!
    var mySectionNumbers = 1
    
    var myDataSource:[String]! = ["Row1","Row2","Row3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.yellowColor()
        mainTableView.dataSource = DrawerTableView()
        mainTableView.delegate = DrawerTableView()
        mainLayer = mainTableView.layer
        
        
        //self.view.addSubview(leftTableView)
        //leftTableView.addSubview(leftTableViewCell)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startButton(sender: UIBarButtonItem) {
        basicAnimation()
        
    }
    
    func basicAnimation(){
        UIView.animateWithDuration(1, animations: {()-> Void in

            
            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.fromValue = 1
            scaleAnimation.byValue = -0.2
            scaleAnimation.duration = 1
        
            self.mainLayer.addAnimation(scaleAnimation, forKey: "scaleAnimation")
            
            self.mainLayer.frame.origin = CGPoint(x: 100, y: 240)
          
            
            
        })

    }
    
    
    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return mySectionNumbers
//    }
//    
//    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return myDataSource.count
//    }
//    
//    
//    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: myReuseIdentifier)
//        let cell = tableView.dequeueReusableCellWithIdentifier(myReuseIdentifier, forIndexPath: indexPath)
//        
//        cell.textLabel?.text = myDataSource[indexPath.row]
//        
//        return cell
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */

}
