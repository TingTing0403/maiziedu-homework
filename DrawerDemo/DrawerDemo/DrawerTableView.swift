//
//  DrawerTableView.swift
//  DrawerDemo
//
//  Created by liu ting on 10/2/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class DrawerTableView:UITableView,UITableViewDataSource,UITableViewDelegate {
    
    var myReuseIdentifier:String! = "mainTableView"
   // var tableViewCell:UITableViewCell!
    
    
    var myTableView:UITableView!
    
    var mySectionNumbers = 1
    
    var myDataSource:[String]! = ["Row1","'Row2","Row3"]
//    
//    init(cellReuseIdentifier:String, myNumberOfSections:Int, tabelViewDataSource:[String],frame:CGRect, tableViewStyle:UITableViewStyle){
//        
//        myReuseIdentifier = cellReuseIdentifier
//        
//        mySectionNumbers = myNumberOfSections
//        
//        myDataSource = tabelViewDataSource
//        super.init(frame: frame, style: tableViewStyle)
//       // myTableView.addSubview(tableViewCell)
//        
//        
//        
//        
//    }

//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return mySectionNumbers
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("mainTableView", forIndexPath: indexPath)
        cell.textLabel?.text = myDataSource[indexPath.row]
        return cell
    }
    
    
    
    
    
    

    
    
}