//
//  ViewController.swift
//  DrawerDemo2
//
//  Created by liu ting on 10/5/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    //得到当前屏幕的宽和高
    var viewWidth:CGFloat!
    var viewHeight:CGFloat!
    //左侧视图的宽和高
    var leftViewWidth:CGFloat!
    var leftViewHeight:CGFloat!
    //主视图缩放比例
    var zoomLevel:CGFloat = 0.6
    //视图
    var mainView:MainView!
   // var leftView:UIView!
    var leftTableView:UITableView!
    
    var leftViewDidShow = false
    
    //tableview datasource
    var members = ["李钟硕","宋智孝","郑容和","姜熙建","孔孝真","金宥利","宋慧乔","金正勋","元彬"]
    var imageSet = ["lizhongshuo","songzhixiao","zhengronghe","gary","kongxiaozhen","jinyouli","songhuiqiao","jinzhengxun","yuanbin"]
    
    
    
    override func viewDidLoad() {
    
        viewHeight = self.view.frame.height
        viewWidth = self.view.frame.width
        //设置主视图
        mainView = MainView(viewWidth: viewWidth, viewHeight: viewHeight)
        super.viewDidLoad()
        let backgroundImageView = UIImageView(image: UIImage(named: "mainViewBackground"))
        backgroundImageView.contentMode = UIViewContentMode.ScaleAspectFill
        self.view.addSubview(backgroundImageView)
        mainView.setMember("lizhongshuo", labelName: "李钟硕")
        
        //主视图添加button
        let moreButton = UIButton(frame: CGRect(x: 10, y: 30, width: 40, height: 40))
        moreButton.setImage(UIImage(named: "more"), forState: UIControlState.Normal)
        moreButton.addTarget(self, action: "getMembersList", forControlEvents: UIControlEvents.TouchUpInside)
        moreButton.alpha = 0.5
        mainView.addSubview(moreButton)
        
        //添加左视图和主视图
        setLeftView()
        self.view.addSubview(mainView)
        //默认启动时隐藏左视图
        self.leftTableView.layer.opacity = 0
        

    }

    //button点击执行动画
    func getMembersList(){
        
        if leftViewDidShow {
            showLeftView()
            hideMainView()
        }
        else{
            showMainView()
            hideLeftView()
        }
        
        leftViewDidShow = !leftViewDidShow
        
    }
    
    //设置左视图函数
    func setLeftView(){

        //左视图宽度为主屏幕宽度的一半
        leftViewWidth = viewWidth/2
        leftViewHeight = viewHeight
        
//        leftView = UIView(frame: CGRect(x: 0 , y: 0, width: leftViewWidth, height: leftViewHeight))
        
        //设置左视图
        leftTableView = UITableView(frame: CGRect(x: 0, y: 0 , width: leftViewWidth, height: leftViewHeight), style: UITableViewStyle.Plain)
        leftTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        leftTableView.backgroundColor = UIColor.clearColor()
        
        self.view.addSubview(leftTableView)
        
        leftTableView.dataSource = self
        leftTableView.delegate = self
       
        leftTableView.registerClass(MyTableViewCell.self, forCellReuseIdentifier: "reusedCell")
        
    }
    
/********************************************
    设置左视图tableview
*********************************************/
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return members.count
    }
    
    //配置单元格
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:MyTableViewCell = tableView.dequeueReusableCellWithIdentifier("reusedCell", forIndexPath: indexPath) as! MyTableViewCell
            
            cell.label.text = members[indexPath.row]
            cell.photoView.image = UIImage(named: imageSet[indexPath.row])
        
            cell.backgroundColor = UIColor.clearColor()
        
            return cell
        
    }
    
    //设置行高
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
            return 100
        
    }
    
    //选中单元格
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        getMembersList()
        mainView.photoTitle = members[indexPath.row]
        mainView.setMember(imageSet[indexPath.row], labelName: members[indexPath.row])
        
    }
    
    
/********************************************
    设置动画效果
*********************************************/
    //隐藏左视图
    func hideLeftView(){
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            self.leftTableView.layer.opacity = 0
            }, completion: nil)
        self.leftTableView.resignFirstResponder()
    }
    //隐藏主视图
    func hideMainView(){
        UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: {()-> Void in
            //视图右移
            self.mainView.center = CGPointMake(self.view.center.x + self.leftViewWidth - self.viewWidth * (1 - self.zoomLevel)/2, self.view.center.y)
            //视图缩小
            self.mainView.transform = CGAffineTransformScale(CGAffineTransformIdentity, self.zoomLevel, self.zoomLevel)
            self.mainView.layer.opacity = 0.8
            }, completion: nil)
    }
    
    //显示左视图
    func showLeftView(){
        UIView.animateWithDuration(1, delay: 0.2, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            
            self.leftTableView.layer.opacity = 1
            self.leftTableView.becomeFirstResponder()
            }, completion: nil)
    }
    
    //显示主视图
    func showMainView(){
        UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            
            self.mainView.center = CGPointMake(self.view.center.x, self.view.center.y)
            
            self.mainView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1)
            self.mainView.layer.opacity = 1
            }, completion: nil)
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    


}

