//
//  MyTableViewCell.swift
//  DrawerDemo2
//
//  Created by liu ting on 10/5/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {
    
    //单元格组件
    var photoViewImage:UIImage!
    var photoView:UIImageView!
    var label:UILabel!
    //图片圆角
    var photoViewRadius:CGFloat = 40
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //单元格图片
        photoView = UIImageView(image: photoViewImage)
        photoView.frame = CGRect(x: 20, y: 10, width: 80, height: 80)
       //图片填充
        photoView.contentMode = UIViewContentMode.ScaleToFill
        photoView.backgroundColor = UIColor.clearColor()
        photoView.layer.cornerRadius = 40
        photoView.clipsToBounds = true
        
        //配置文本属性
        label = UILabel(frame: CGRect(x: 110, y: 40, width: 60, height: 20))
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont.boldSystemFontOfSize(15)
        label.textColor = UIColor.whiteColor()
        
        //添加子视图
        self.addSubview(photoView)
        self.addSubview(label)
    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
