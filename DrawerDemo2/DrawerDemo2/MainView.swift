//
//  MainView.swift
//  DrawerDemo2
//
//  Created by liu ting on 10/5/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class MainView: UIView {

    var mainView:UIView?
    var mainViewBackground = UIImage(named: "")
    
    var mainViewPhotoView:UIImageView?
    var mainViewPhoto:UIImage?
    var mainViewLabel = UILabel(frame: CGRect(x: 50, y: 420, width: 300, height: 150))
    
    var viewWidth:CGFloat = 400
    var viewHeight:CGFloat = 600
    var photoTitle = "defaultuserphoto"
    var detail = "None"
    
    
    init(viewWidth:CGFloat, viewHeight:CGFloat){
        
        self.viewHeight = viewHeight
        self.viewWidth = viewWidth
        super.init(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
        
        
        self.backgroundColor = UIColor.clearColor()
        
        
        
    }
    
    //显示某成员
    func setMember(photoName:String, labelName:String){
        detail = labelName
        photoTitle = photoName
        mainViewPhotoView?.removeFromSuperview()
        mainViewLabel.removeFromSuperview()
        setUpPhotoView()
        setUpLabel()
    }
    
    //设置label
    func setUpLabel(){
        mainViewLabel.frame =  CGRect(x: 0, y: 0, width: 300, height: 100)
        mainViewLabel.text = detail
        mainViewLabel.font = UIFont.boldSystemFontOfSize(22)
        mainViewLabel.textColor = UIColor.whiteColor()
        mainViewLabel.numberOfLines = 0
        mainViewLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        mainViewLabel.sizeToFit()
        mainViewLabel.center.x = self.center.x
        mainViewLabel.frame.origin.y = (mainViewPhotoView?.frame.maxY)! + 20
        self.addSubview(mainViewLabel)
    }
    
    //设置图片
    func setUpPhotoView(){
    
        mainViewPhoto = UIImage(named:photoTitle)!
        mainViewPhotoView = UIImageView(image: mainViewPhoto)
        mainViewPhotoView!.frame = CGRect(x: 50, y: 80, width: 300, height: 300)
        mainViewPhotoView!.center = self.center
        mainViewPhotoView!.contentMode = UIViewContentMode.ScaleAspectFill
        mainViewPhotoView!.layer.cornerRadius = 150
        mainViewPhotoView!.clipsToBounds = true
        
        self.addSubview(mainViewPhotoView!)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
  

}
