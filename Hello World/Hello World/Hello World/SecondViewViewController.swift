//
//  SecondViewViewController.swift
//  Hello World
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class SecondViewViewController: UIViewController {
    
    @IBAction func `switch`(sender: UISwitch) {
        sender.backgroundColor = UIColor.redColor()
    }
    
    @IBOutlet weak var lable2ndView: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lable2ndView.text = "Hello World"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
