//
//  ViewController.swift
//  Hello World
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func clickButton(sender: UIButton) {
        
        sender.backgroundColor = UIColor.redColor()
        print("User Clicked Button")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Hello World~"
        print("ViewDidLoad")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        print("view will appear")
    }
    override func viewDidAppear(animated: Bool) {
        print("view did appear")
    }
    
    
    
    override func viewWillDisappear(animated: Bool) {
        print("view will disappear")
    }
    
    
    
        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

