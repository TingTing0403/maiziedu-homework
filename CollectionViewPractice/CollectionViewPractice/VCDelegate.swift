//
//  VCDelegate.swift
//  CollectionViewPractice
//
//  Created by liu ting on 9/28/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import Foundation

protocol PassValue {
    func passValue(imageName: String)
}