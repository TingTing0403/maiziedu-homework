//
//  SecondViewController.swift
//  CollectionViewPractice
//
//  Created by liu ting on 9/28/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var delegate: PassValue?
    
    let textFieldText: UITextField! = UITextField(frame: CGRect(x:100 , y: 100, width: 50, height: 30))
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldText.backgroundColor = UIColor.yellowColor()
        self.view.addSubview(textFieldText)
        self.view.backgroundColor = UIColor.whiteColor()
        self.navigationItem.title = "VC2"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "返回", style: UIBarButtonItemStyle.Plain, target: self, action: "goBack")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "add", style: UIBarButtonItemStyle.Plain, target: self, action: "addButton2Clicked")
        
        
        // Do any additional setup after loading the view.
    }
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }

    func addButton2Clicked(){
        
      //  let VC1 = ViewController()
        if textFieldText.text != nil {
        self.delegate?.passValue(textFieldText.text!)
        }
       
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   // override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        
        
        // Pass the selected object to the new view controller.
  //  }
    

}
