//
//  ViewController.swift
//  CollectionViewPractice
//
//  Created by liu ting on 9/27/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController:UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,PassValue {
    

    
    @IBOutlet weak var navigationItemVC1: UINavigationItem!
    
    var imageSet = ["1","2","3","4","5","6","7","8","9","10"]

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        navigationItemVC1.title = "VC1"
        navigationItemVC1.rightBarButtonItem = UIBarButtonItem(title: "add", style: UIBarButtonItemStyle.Plain, target: self, action: "addButtonClicked")
        
        
        
    }
    
    func passValue(imageName: String) {
        imageSet.append(imageName)
        self.collectionView.reloadData()
    }
    
    
    func addButtonClicked(){
        let VC2 = SecondViewController()
        
        VC2.delegate = self
        
        self.navigationController?.pushViewController(VC2, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //section数目
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    //cell数目
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSet.count
    }
    
    //自定义reuseable cell
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath)
        
        let cellImage = cell.viewWithTag(1) as! UIImageView
        cellImage.image = UIImage(named: imageSet[indexPath.row])
        cellImage.contentMode = UIViewContentMode.ScaleAspectFit
        return cell
    }
    
    //设定cell的大小
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let size1 = CGSize(width: 80, height: 80)
        let size2 = CGSize(width: 150, height: 150)
        if indexPath.row%2 == 0 {
            return size1
        }
        else{
            return size2
        }
    }
    
   
    
    


}

