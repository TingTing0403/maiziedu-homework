//
//  ViewController.swift
//  AnimationPractice
//
//  Created by liu ting on 9/29/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var animationView: UIView!
    
    @IBOutlet weak var secondAnimationView: UIView!
    var originalFrame: CGRect?
    
    var myFirstSubview: UIView = UIView(frame: CGRect(x: 5, y: 5, width: 30, height: 30))
    var myFirstSubviewFrame: CGRect?
    override func viewDidLoad() {
        super.viewDidLoad()
        //获取动画view当前frame
        originalFrame = self.animationView.frame
        myFirstSubviewFrame = myFirstSubview.frame
        myFirstSubview.backgroundColor = UIColor.purpleColor()
        animationView.addSubview(myFirstSubview)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func startButton(sender: UIButton) {
        self.replaceViewAnimation()
    }
    
    //MARK：恢复初始状态
    @IBAction func fallbackButton(sender: UIButton) {
        
        self.animationView.frame = originalFrame!
        self.animationView.layer.removeAllAnimations()
        self.myFirstSubview.frame = myFirstSubviewFrame!
        
    }
    
    //MARK:基础动画
    func basicAnimation(){
        UIView.animateWithDuration(1, animations: {()->Void in
         self.animationView.frame.origin.x += 100
        })
    }
    
    func optionalAnimation(){
        UIView.animateWithDuration(1, animations: {()->Void in
            self.animationView.frame.origin.x += 100
            }, completion: {(finish)-> Void in
            print("finished")
        })
    }
    
    
    //MARK: UIViewAnimationOperations,
    func proAnimation(){
        UIView.animateWithDuration(1.0, delay: 1.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {()->Void in
            self.animationView.frame.origin.y += 300
            //MARK: Nest
            UIView.animateWithDuration(10, delay: 0, options:[UIViewAnimationOptions.OverrideInheritedDuration,UIViewAnimationOptions.OverrideInheritedCurve,UIViewAnimationOptions.Repeat,UIViewAnimationOptions.CurveEaseIn] , animations: {()->Void in
                
                self.secondAnimationView.frame.origin.y += 300
                
                }, completion: {(finish)->Void in
                    print("second finished")
            })
            
            }, completion: {(finish)-> Void in
                
               
        
                print("finished")
        })
    }
    
    //MARK: Beigin/ commit
    func beginCommitAnimations(){
        
        UIView.beginAnimations("customAmination", context: nil)
        UIView.setAnimationDuration(2.0)
        UIView.setAnimationDelay(1.0)
        UIView.setAnimationRepeatAutoreverses(true)
        UIView.setAnimationRepeatCount(10)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationWillStartSelector("animationDidStart:")
        UIView.setAnimationDidStopSelector("animationDidStop:finished:")
        self.animationView.backgroundColor = UIColor.magentaColor()
        UIView.commitAnimations()
    }
    
    //MARK：转场特效 nest
    func viewAnimation(){
        
        UIView.transitionWithView(animationView, duration: 2.0, options: [UIViewAnimationOptions.TransitionCrossDissolve,UIViewAnimationOptions.AllowAnimatedContent], animations: {()->Void in
            
            
            UIView.animateWithDuration(5.0, delay: 0, options: [UIViewAnimationOptions.CurveLinear,UIViewAnimationOptions.OverrideInheritedDuration], animations: {()-> Void in
                self.myFirstSubview.frame.origin.x += 200
                
                }, completion: nil)
            
            }, completion: nil)
    }
    
    //MARK:视图替换
    
    func replaceViewAnimation(){
        let myVeiw3 = UIView(frame: CGRect(x: 50, y: 400, width: 100, height: 50))
        myVeiw3.backgroundColor = UIColor.greenColor()
        
        UIView.transitionFromView(secondAnimationView, toView: myVeiw3, duration: 2, options: UIViewAnimationOptions.TransitionCurlDown, completion: {(finished)-> Void in
            print("transition from view finished")
        })
    }
    
    
    //delegate
    override func animationDidStart(anim: CAAnimation) {
        print("animationStart")
    }
    //delegate
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        print("animationStopped")
    }
    
    
    //MARK:
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

