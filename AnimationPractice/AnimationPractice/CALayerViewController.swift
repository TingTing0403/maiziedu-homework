//
//  CALayerViewController.swift
//  AnimationPractice
//
//  Created by liu ting on 10/1/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class CALayerViewController: UIViewController {

    var myLayer:CALayer!
    @IBAction func startAnimation(sender: UIButton) {
        
        //implicitAnimation()
       // myBasicAnimation()
        //keyAnimation()
        springAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //layerTest()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func layerTest(){
        
        myLayer = CALayer()
        //MARK: CALayer color 是 CGColor类型
        myLayer.backgroundColor = UIColor.orangeColor().CGColor
        myLayer.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        myLayer.borderColor = UIColor.blueColor().CGColor
        myLayer.borderWidth = 3.0
        myLayer.cornerRadius = 20
        myLayer.shadowColor = UIColor.lightGrayColor().CGColor
        myLayer.shadowOffset = CGSize(width: 5, height: 5)
        myLayer.shadowOpacity = 0.5
        self.view.layer.addSublayer(myLayer)
        
    }
    
    func implicitAnimation(){
        myLayer.frame.origin.y += 100
    }
    
    //显式动画
    func myBasicAnimation(){
        
        let myPhotoView = UIImageView(image: UIImage(named: "gary"))
        myPhotoView.frame = CGRect(x: 200, y: 200, width: 100, height: 100)
        self.view.addSubview(myPhotoView)
        //MARK:指定动画对象
        let myBasicLayer = myPhotoView.layer
        //MARK:设定动画效果
        let basicAnimation = CABasicAnimation(keyPath: "transform.scale")
        basicAnimation.fromValue = 1
        basicAnimation.toValue = 2
        basicAnimation.duration = 5
        basicAnimation.repeatCount = MAXFLOAT
        basicAnimation.autoreverses = true
        
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = 0
        opacityAnimation.toValue = 1
        opacityAnimation.duration = 5
        opacityAnimation.repeatCount = MAXFLOAT
        opacityAnimation.autoreverses = true
        
        let rotatingAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotatingAnimation.fromValue = 0
        rotatingAnimation.toValue = M_PI * 2
        rotatingAnimation.duration = 5
        rotatingAnimation.repeatCount = MAXFLOAT
        rotatingAnimation.autoreverses = true
        //MARK:添加动画效果
        
        myBasicLayer.addAnimation(basicAnimation, forKey: "basicAnimation")
        myBasicLayer.addAnimation(opacityAnimation, forKey: "opacityAnimation")
        myBasicLayer.addAnimation(rotatingAnimation, forKey: "rotatingAnimation")
    }
    
    func keyAnimation(){
        let myPhotoView = UIImageView(image: UIImage(named: "gary"))
        myPhotoView.frame = CGRect(x: 200, y: 200, width: 80, height: 80)
        self.view.addSubview(myPhotoView)
        
        let keyAnimation = CAKeyframeAnimation(keyPath: "position")
        
        let value0 = NSValue(CGPoint: myPhotoView.layer.position)
        let value1 = NSValue(CGPoint: CGPoint (x:myPhotoView.layer.position.x, y:myPhotoView.layer.position.y + 200))
        let value2 = NSValue(CGPoint: CGPoint (x:myPhotoView.layer.position.x - 100, y:myPhotoView.layer.position.y + 200))
        let value3 = NSValue(CGPoint: CGPoint(x:myPhotoView.layer.position.x - 100, y:myPhotoView.layer.position.y))
        
        keyAnimation.values = [value0, value1, value2, value3, value0]
        keyAnimation.duration = 10
        keyAnimation.autoreverses = false
        keyAnimation.repeatCount = MAXFLOAT
        keyAnimation.keyTimes = [0.0, 0.4, 0.5, 0.6, 0.7]
        let tf0 = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        let tf1 = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        let tf2 = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let tf3 = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        keyAnimation.timingFunctions = [tf0, tf1, tf2, tf3]
        
        myPhotoView.layer.addAnimation(keyAnimation, forKey: "keyAnimation")
    }
    
    func springAnimation(){
        let myPhotoView = UIImageView(image: UIImage(named: "gary"))
        myPhotoView.frame = CGRect(x: 50, y: 200, width: 80, height: 80)
        self.view.addSubview(myPhotoView)
        
        UIView.animateWithDuration(2, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: UIViewAnimationOptions.CurveEaseOut, animations: {()->Void in
            
            myPhotoView.layer.position.x += 100
            
            
            }, completion: {(finished)-> Void in
                myPhotoView.layer.opacity = 0
        
        })
        
    }
    


}
