//
//  ViewController.swift
//  AnimatingDots
//
//  Created by liu ting on 10/8/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
 
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var newLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label1.layer.borderColor = UIColor.greenColor().CGColor
        label1.layer.borderWidth = 1
        print(label1.frame)
        print(label1.layer.frame)
        
        addAnimatingDot(newLabel, numberOfDots: 2, animatingTime: 10)
        addAnimatingDot(label1, numberOfDots: 10, animatingTime: 10)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func addAnimatingDot(aimView:UIView, numberOfDots:Int, animatingTime: Double){
       
        
        for i in 1...numberOfDots {
            var dotView = UIView()
            //MARK:画点
            dotView = getDot(aimView.layer.borderColor!)
            
        //MARK:设定动画
            let animatingDot = CAKeyframeAnimation(keyPath: "position")
        
            let value0 = NSValue(CGPoint: dotView.center)
            let value1 = NSValue(CGPoint: CGPoint (x:aimView.frame.width, y:0))
            let value2 = NSValue(CGPoint: CGPoint (x:aimView.frame.width, y:aimView.layer.frame.height))
            let value3 = NSValue(CGPoint: CGPoint(x:0, y:aimView.layer.frame.height))
        
            animatingDot.values = [value0,value1,value2,value3,value0]
            animatingDot.duration = animatingTime
            animatingDot.calculationMode = kCAAnimationPaced//匀速
            animatingDot.repeatCount = MAXFLOAT
            animatingDot.autoreverses = false
        
        
            aimView.addSubview(dotView)
            //MARK:动画执行的推迟时间
            animatingDot.timeOffset = CFTimeInterval.abs(animatingTime * Double(i) / Double(numberOfDots))
            dotView.layer.addAnimation(animatingDot, forKey: "animatingDot")
        }
    }
    
    //MARK:画点
    func getDot(dotColor:CGColor)->UIView{
        
            let dotView = UIView()
            dotView.center = CGPoint(x: 0, y: 0)
            dotView.frame.size = CGSize(width: 4, height: 4)
            dotView.layer.cornerRadius = 2
            dotView.clipsToBounds = true
            dotView.layer.backgroundColor = dotColor
        
            return dotView
        
       
    }

    
    
}

