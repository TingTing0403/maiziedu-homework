//
//  mainViewController.swift
//  DrawerDemo1
//
//  Created by liu ting on 10/3/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class mainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    var leftView: UIView!
    var mainView: UIView!
    var leftLayer: CALayer!
    var mainLayer: CALayer!
    let leftLayerColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).CGColor
    let whiteColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0).CGColor
    var showLeft = false
    let zoomLevel:CGFloat = 0.8
    var mainViewHeight:CGFloat!
    var mainViewWidth:CGFloat!
    
    var leftViewHeight:CGFloat!
    var leftViewWidth: CGFloat!
    
    var myToolBar: UIToolbar!
    
    
    
    var leftViewDataSource = ["开通会员","QQ钱包","网上营业厅","个性装扮","我的收藏","我的相册","我的文件"]
    var mainViewDataSource = ["Gary","Yong","Jessica","Tina"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainViewHeight = self.view.frame.height
        mainViewWidth = self.view.frame.width
        leftViewHeight = mainViewHeight
        leftViewWidth = mainViewWidth * 0.7
        leftView = setTableView("leftView",myTag: 1)
        mainView = setTableView("mainView",myTag: 2)
        
        let backGroundImage = UIImageView(image: UIImage(named: "backgroundImage"))
        backGroundImage.frame = self.view.frame
        backGroundImage.contentMode = UIViewContentMode.ScaleAspectFill
        self.view.addSubview(backGroundImage)
        self.view.addSubview(leftView)
        self.view.addSubview(mainView)
        
       // hideLeftView()
    }

    func setTableView(cellReuseIdentifier:String, myTag:Int)->UIView{
        var myView: UIView!
        var myHeaderView: UIView!
        var myTableView:UITableView!
        if myTag == 1{
            myView = UIView(frame: CGRect(x: 0 , y: 0, width: leftViewWidth, height: leftViewHeight))
            myTableView = UITableView(frame: CGRect(x: 0, y: 180, width: leftViewWidth, height: leftViewHeight - 180), style: UITableViewStyle.Plain)
            myTableView.separatorStyle = UITableViewCellSeparatorStyle.None
            myTableView.backgroundColor = UIColor.clearColor()
           // myTableView.layer.backgroundColor = leftLayerColor

            
            
            myHeaderView = UIView(frame: CGRect(x: 0, y: 80, width: leftViewWidth, height: 100))
            
            let gradientLayer = CAGradientLayer()
            gradientLayer.frame = myHeaderView.frame
          //  gradientLayer.colors = [whiteColor,leftLayerColor]
            CGGradientRef
            
            
          //  gradientLayer.opacity = 0.5
           // myHeaderView.backgroundColor = UIColor.greenColor()
            myView.addSubview(myHeaderView)
           // myView.layer.addSublayer(gradientLayer)
        }
        if myTag == 2{
            myView = UIView(frame: CGRect(x: 0, y: 0, width:mainViewWidth, height: mainViewHeight))
            myTableView = UITableView(frame: CGRect(x: 0, y: 100, width: mainViewWidth, height: mainViewHeight - 80), style: UITableViewStyle.Plain)
            
            myToolBar = UIToolbar(frame: CGRect(x: 0, y: 20, width: mainViewWidth, height: 100))
            let toolbBarButton = UIBarButtonItem(image: UIImage(named: "defaultuserphoto"), style: UIBarButtonItemStyle.Plain, target: self, action: "userPhotoClicked")
            
            myToolBar.setItems([toolbBarButton], animated: false)
            myView.addSubview(myToolBar)
            
            
        }
        myTableView.dataSource = self
        myTableView.delegate = self
        myTableView.tag = myTag
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        myView.addSubview(myTableView)
       
        return myView
        
    }
    
    func userPhotoClicked(){
        leftLayer = leftView.layer
        mainLayer = mainView.layer
        showLeft = !showLeft
        
        if showLeft{
            showLeftView()
            hideMainView()
        }
        else {
            showMainView()
            hideLeftView()
        }
        
            
        
    
        
        
    }
   
    func hideLeftView(){
        UIView.animateWithDuration(1, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            
//            self.leftView.center = CGPointMake(-self.leftViewWidth * self.zoomLevel / 2 , self.view.center.y)
//            self.leftView.transform = CGAffineTransformScale(CGAffineTransformIdentity, self.zoomLevel, self.zoomLevel)
            self.leftView.layer.opacity = 0
            
            }, completion: nil)
    }
    
    func hideMainView(){
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            
            self.mainView.center = CGPointMake(self.view.center.x + self.leftViewWidth - self.mainViewWidth * (1 - self.zoomLevel)/2, self.view.center.y)
            
            self.mainView.transform = CGAffineTransformScale(CGAffineTransformIdentity, self.zoomLevel, self.zoomLevel)
            
            }, completion: nil)
    }
    
    func showLeftView(){
        UIView.animateWithDuration(1, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in

//            self.leftView.center = CGPointMake(self.leftViewWidth/2, self.view.center.y)
//            self.leftView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1)
            self.leftView.layer.opacity = 1
            }, completion: nil)
    }
    
    func showMainView(){
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {()-> Void in
            
            self.mainView.center = CGPointMake(self.view.center.x, self.view.center.y)
            
            self.mainView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1)
            
            }, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1{
         return leftViewDataSource.count
        }
        else{
         return mainViewDataSource.count
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell!
        if tableView.tag == 1
        {   cell = tableView.dequeueReusableCellWithIdentifier("leftView", forIndexPath: indexPath)
        
            cell.textLabel?.text = leftViewDataSource[indexPath.row]
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
            cell.backgroundColor = UIColor.clearColor()
            cell.textLabel?.textColor = UIColor.whiteColor()
            
            
            }
        else {
            
            cell = tableView.dequeueReusableCellWithIdentifier("mainView", forIndexPath: indexPath)
            
            cell.textLabel?.text = mainViewDataSource[indexPath.row]
            cell.imageView?.image = UIImage(named: "defaultuserphoto")
            
        }
        return cell
    
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView.tag == 1 {
            return 60
        }
        else {
            return 50
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
