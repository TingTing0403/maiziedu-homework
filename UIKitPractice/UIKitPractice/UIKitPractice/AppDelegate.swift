//
//  AppDelegate.swift
//  UIKitPractice
//
//  Created by liu ting on 9/11/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //setup a window
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.rootViewController = UIViewController()
        self.window?.backgroundColor = UIColor.redColor()
        self.window?.makeKeyAndVisible()
        //add subview
        let rootView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 600))
        rootView.backgroundColor = UIColor.whiteColor()
        self.window?.addSubview(rootView)
        //add a button
        let button = UIButton(frame: CGRect(x: 20, y: 10, width: 100, height: 44))
        button.backgroundColor = UIColor.blueColor()
        button.setTitle("I am a button", forState: UIControlState.Normal )
        button.addTarget(self, action: "clickButton:", forControlEvents: UIControlEvents.TouchUpInside)
        rootView.addSubview(button)
        //add a label
        let label = UILabel(frame: CGRect(x: 20, y: 100, width: 100, height: 44))
        label.backgroundColor = UIColor.whiteColor()
        label.text = "I am a label"
        label.tag = 2
        rootView.addSubview(label)
        
       return true
    }
    
    @IBAction
    func clickButton (sender : UIButton) {
        
        sender.backgroundColor = UIColor.greenColor()
        let label = self.window?.viewWithTag(2) as! UILabel
        label.text = "i am a label"
        label.backgroundColor = UIColor.blueColor()
        
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

