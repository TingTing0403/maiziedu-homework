//
//  ViewController.swift
//  SandBoxTest
//
//  Created by liu ting on 10/15/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let libraryPath = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.LibraryDirectory, NSSearchPathDomainMask.UserDomainMask, false).first! as String
//        
//        print(libraryPath)
//        
//        let temp = NSUserDefaults()
//        temp.setObject("user defaults", forKey: "name")
//        
//        print("\(NSHomeDirectory())")
    
       // saveFile()
       // readWithFile()
        //saveWithNSUserDefault()
       // readWithNSUserDefault()
        saveWithNSKeyedArchiver()
        readWithNSKeyedUnArchiver()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveFile(){
        
        //1.获得沙盒根路径
        let home = NSHomeDirectory() as NSString
        
        //2.获得Documents路径
        let docPath = home.stringByAppendingPathComponent("Documents") as NSString
        //3.获取文本文件路径
        let filePath = docPath.stringByAppendingPathComponent("data.plist")
        
        let dataSource = NSMutableArray()
        dataSource.addObject("number1")
        dataSource.addObject("number2")
        dataSource.addObject("number3")
        dataSource.addObject("number4")
        dataSource.addObject("number5")
        
        dataSource.writeToFile(filePath, atomically: true)
        
        print("\(filePath)")
    }
    
    func readWithFile(){
        
        //1.获取沙盒路径
        let home = NSHomeDirectory() as NSString
        //2. 获得Documents路径，使用NSString对象的stringByAppendingPathComponent()拼接路径
        let docPath = home.stringByAppendingPathComponent("Documents") as NSString
        //3. 获取文本路径
        let filePath = docPath.stringByAppendingPathComponent("data.plist")
        let dataSource = NSArray(contentsOfFile: filePath)
        
        print("\(dataSource)")
        
    }
    
    //使用NSUserDefault存储数据
    func saveWithNSUserDefault(){
        //利用NSUserDefault存储数据
        let userDefault = NSUserDefaults.standardUserDefaults()
        //存储数据
        userDefault.setObject("衣带渐宽终不悔", forKey: "name")
        //同步数据
        userDefault.synchronize()
    }
    func readWithNSUserDefault(){
        let userDefault = NSUserDefaults.standardUserDefaults()
        let name = userDefault.objectForKey("name") as! NSString
        print(name)
    }
    
    //归档数据存储，需要实现NSCoding协议
    
    func saveWithNSKeyedArchiver(){
        let home = NSHomeDirectory() as NSString
        let docPath = home.stringByAppendingPathComponent("Documents") as NSString
        let filePath = docPath.stringByAppendingPathComponent("book.data")
       
        let book = Book(name: "天龙八部", pages: 100)
        
        NSKeyedArchiver.archiveRootObject(book, toFile: filePath)
    }
    
    func readWithNSKeyedUnArchiver(){
        let home = NSHomeDirectory() as NSString
        let docPath = home.stringByAppendingPathComponent("Documents") as NSString
        let filePath = docPath.stringByAppendingPathComponent("book.data")
        let book = NSKeyedUnarchiver.unarchiveObjectWithFile(filePath) as! Book
        
        print("\(book.name),\(book.pages)")
    }


}

class Book {
    var name:String
    var pages:Int
    init(name: String, pages:Int){
        self.name = name
        self.pages = pages
    }
    
    
    
}

