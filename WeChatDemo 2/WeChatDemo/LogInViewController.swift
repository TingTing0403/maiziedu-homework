//
//  LogInViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/20/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit
//用户名 liuting，密码 liuting，暂不支持动态修改
class LogInViewController: UIViewController {
    //用户名输入框
    @IBOutlet weak var userName: UITextField!
    
    @IBAction func userNameEndEdit(sender: UITextField) {
      
            resignFirstResponder()
      
    }
    //密码输入框
    @IBOutlet weak var password: UITextField!
    
    @IBAction func passwordEndEdit(sender: UITextField) {
      
            resignFirstResponder()
       
    }
    //登录按钮
    @IBAction func logInButton(sender: UIButton) {
        
        if userName.text == "liuting" && password.text == "liuting" {
            print("log in successfully")
            let tabBarViewController = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarViewController")
            self.navigationController?.pushViewController(tabBarViewController!, animated: true)
            
        }
        else {
            print("invalid username or password")
        }
    }
    //去注册
    @IBAction func goSignIn(sender: UIButton) {
        let signInView = self.storyboard?.instantiateViewControllerWithIdentifier("SignInView")
        self.navigationController?.pushViewController(signInView!, animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    //一旦离开登录界面，则清空用户名和密码
    override func viewDidDisappear(animated: Bool) {
        userName.text = ""
        password.text = ""
    }
    

    
}
