//
//  WelcomeViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/19/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController, UIScrollViewDelegate {
    
    //设置背景图片
    @IBOutlet weak var scrollViewBackground: UIScrollView!
    
    func setUpBackgroundPage (){
        //获取当前view的宽和高
        let viewWidth = self.view.frame.width
        let viewHight = self.view.frame.height
        
        //添加4个图片
        
        for i in 0...3 {
            let pic = UIImage(named:"pic\(i).png")
            if (pic != nil) {
            let picView = UIImageView(image: pic!)
            picView.frame = CGRect(x: viewWidth * CGFloat(i), y: 0, width: viewWidth, height: viewHight)
                
            scrollViewBackground.addSubview(picView)
                
            }
            else{
                print("pic returned nil")
            }
        }
        
        //设置scrollview尺寸及offset
       scrollViewBackground.contentOffset = CGPoint(x:0, y:0)
       scrollViewBackground.contentSize = CGSize(width: viewWidth*4, height: viewHight)
        
    }
    //设置page View
    @IBOutlet weak var pageIndicator: UIPageControl!
   

    
    //点击翻页方法实现
    func pageValueChanged(sender: UIPageControl) {
        let currentPageX = CGFloat(pageIndicator.currentPage) * self.view.frame.width
        scrollViewBackground.scrollRectToVisible(CGRect(x: currentPageX, y: 0, width: self.view.frame.width, height: self.view.frame.height), animated: true)
    }
    //翻页 pageview改变
    func scrollViewDidScroll(scrollView: UIScrollView) {
        pageIndicator.currentPage = Int(scrollViewBackground.contentOffset.x/self.view.frame.width)
    }
    
    @IBAction func logInButton(sender: UIButton) {
    }
    
    @IBAction func SignInButton(sender: UIButton) {
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //设置背景图片
        setUpBackgroundPage()
        //点击翻页
        pageIndicator.addTarget(self, action: "pageValueChanged:", forControlEvents: UIControlEvents.ValueChanged)
        //设置代理
        scrollViewBackground.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

}
