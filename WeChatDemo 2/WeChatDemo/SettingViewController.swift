//
//  SettingViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/20/15.
//  Copyright © 2015 liu ting. All rights reserved.
//


import UIKit

class SettingViewController: UITableViewController{
    
    //设置界面 datasource
    let setting = ["section0":["修改头像"],"section1":["修改用户名","修改密码"],"section2":["退出登录"]]

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    //section 3个
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 3
    }
    //每个section中的行数为当前section的array的count
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (setting["section\(section)"]?.count)!
    }
    //每个section设置一个header
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 20))
        
        headerView.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.1)
        
        return headerView
    }
    //设置cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "SettingTable"
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifier)
        cell.textLabel?.text = setting["section\(indexPath.section)"]?[indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("setting section\(indexPath.section) row\(indexPath.row)")
    }
    
    }
