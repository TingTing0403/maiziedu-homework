//
//  ChatUIView.swift
//  WeChatDemo
//
//  Created by liu ting on 9/22/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

//ChatUIView类用于生成一个对话泡泡
import UIKit

class ChatUIView: UIView {
    
    var x:CGFloat = 0 //view的x坐标
    var y:CGFloat = 0 //view的y坐标
    
    var width:CGFloat = 0 //设定view宽度
    var height: CGFloat = 60 //设定view高度
    
    var maxWidth: CGFloat = 175//设定对话框的最大宽度
    
    var selfMessage = false
    var message:String = "" //对话框内容
    var userPhotoView = UIImageView(image: UIImage(named: "defaultuserphoto"))
    
    var font = UIFont.boldSystemFontOfSize(15)
    
    
    var imageInterval:CGFloat = 10
    var rowHeight:CGFloat = 60
    var extensionSize:CGFloat = 30
    var userPhotoSize:CGFloat = 40
    
    var backgroundImage = UIImage()
    var newBackgroundImage = UIImage()
    var backgroundView = UIImageView()
    
    
    
    //初始化UIVIEW
    init(y:CGFloat, width:CGFloat, selfMessage:Bool, message:String,userPhoto:UIImage?){
        super.init(frame: CGRect(x:0, y: y, width: width, height:rowHeight))
        self.y = y
        self.width = width
        self.selfMessage = selfMessage
        self.message = message
        
        //如果初始化的userphoto不为nil，则替换成新的userphoto
        if userPhoto != nil {
            self.userPhotoView = UIImageView(image: userPhoto)
        }
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setChatView()->UIView{
        //label的x位置和y位置比较重要,宽和高随文字自适应，width为文本框的最大宽度，height随意设置
        var chatLabel = UILabel()
        //如果是己方消息框x位置留白一点，如果是对方消息框，x位置留白多一点
        if selfMessage == true{
            chatLabel = UILabel(frame: CGRect(x: imageInterval, y:imageInterval, width: maxWidth, height: 40))
        }else{
            chatLabel = UILabel(frame: CGRect(x: imageInterval*2, y: imageInterval, width: maxWidth, height: 40))
            
        }
        chatLabel.text = message
        //字体
        chatLabel.font = UIFont.boldSystemFontOfSize(15)
        //以下三行实现label自适应文字多少，并自动换行
        chatLabel.numberOfLines = 0
        chatLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        chatLabel.sizeToFit()
        
        
        if selfMessage == true{
            //如果是自己发的消息，消息框背景图片为backgroundphoto2
            backgroundImage = UIImage(named: "backgroundPhoto2")!
        }
        else {
            //如果是对方发的消息，消息框背景图片为backgroundphoto
            backgroundImage = UIImage(named: "backgroundPhoto1")!
        }
        //设置图片局部复制填充
        newBackgroundImage = backgroundImage.resizableImageWithCapInsets(UIEdgeInsets(top: 25, left: 30, bottom: 10, right: 30), resizingMode: UIImageResizingMode.Tile)
        
        //图片添加至imageView
        backgroundView = UIImageView(image: newBackgroundImage)
        //如果是己方消息框，backgroundView的位置是（width-用户头像-label.width,图片间留白)，长和宽分别比label的长宽大一点点。用户头像位置是(width-头像宽度,图片留白)
        if selfMessage == true
        {
            
            backgroundView.frame = CGRect(x: width - extensionSize - userPhotoSize - 2*imageInterval - chatLabel.frame.width, y: imageInterval, width: chatLabel.frame.width + extensionSize, height: chatLabel.frame.height + imageInterval*2)
            userPhotoView.frame = CGRect(x: width - imageInterval - userPhotoSize, y: imageInterval, width: userPhotoSize, height: userPhotoSize)
            
        }
            //如果是对方消息框，backgroundview的位置是(头像宽度,10),长和宽分别比label大一点。
        else{
            backgroundView.frame = CGRect(x: userPhotoSize + 2*imageInterval, y: imageInterval, width: chatLabel.frame.width + extensionSize , height: chatLabel.frame.height + imageInterval*2)
            userPhotoView.frame = CGRect(x:imageInterval, y: imageInterval, width: userPhotoSize, height: userPhotoSize)
            
        }
        //label添加至imageview
        backgroundView.addSubview(chatLabel)
        
        
        //最终view的大小长为屏幕宽度，高为60
        let finalView = UIView(frame: CGRect(x: 0, y: y, width: width, height: backgroundView.frame.height))
        
        finalView.addSubview(backgroundView)
        
        finalView.addSubview(userPhotoView)
        
        return finalView
    }
    

    
    
    
    
    
 }
