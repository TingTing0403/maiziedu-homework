//
//  ChatViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/22/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {
    /*
    
    tips: 运行设备为5s，屏幕大小设置为320*586，不支持多尺寸屏幕的适配
*/
    
    var currentMaxY:CGFloat = 454//当前泡泡的最大坐标
    var contentMaxY:CGFloat = 454//当前scroll view content最大坐标
    var viewWidth:CGFloat = 320//屏宽
    let keyBoardHeight:CGFloat = 250//键盘高度
    var viewHeight:CGFloat = 454//scrollView高度
    
    
    //设定一个scrollview，高度 = 屏高-导航栏高度-底栏高度
    var chatScrollView = UIScrollView(frame: CGRect(x: 0, y: 64, width: 320, height: 454))
    
    //定义底部工具栏，文本输入框，button
    var typingField = UITextField(frame: CGRect(x: 20, y: 10, width: 240, height: 30))
    
    var typingDoneButton = UIButton(frame: CGRect(x:260 , y: 10, width: 40, height: 30))
    
    var backgroundImage = UIImageView(frame: CGRect(x: 0, y: 518, width: 320, height: 50))
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        viewWidth = self.view.frame.width
        viewHeight = self.view.frame.height - backgroundImage.frame.height - 64
       // chatScrollView.backgroundColor = UIColor.greenColor()
        
        //设置背景imageview
        backgroundImage.backgroundColor = UIColor.blackColor()
        backgroundImage.alpha = 0.5
        backgroundImage.userInteractionEnabled = true//重要！
        
        //自定义textfield
      //  typingField.backgroundColor = UIColor.yellowColor()
        typingField.borderStyle = UITextBorderStyle.RoundedRect

        
        //textfield事件，编辑结束resignfirst responder
        typingField.addTarget(self, action: "texFieldDidEndOnExit:", forControlEvents: UIControlEvents.EditingDidEndOnExit)
        
        
        //自定义button
     //   typingDoneButton.backgroundColor = UIColor.redColor()
        typingDoneButton.setTitle("发送", forState: UIControlState.Normal)
        typingDoneButton.userInteractionEnabled = true
        
        //点击事件
        typingDoneButton.addTarget(self, action: "sendMessageButton:", forControlEvents: UIControlEvents.TouchUpInside)
        
       
      
        //创建一个对方消息泡泡
        let otherMessage = ChatUIView(y: 0 , width: viewWidth, selfMessage: false, message: "你好", userPhoto: nil).setChatView()
        
        //泡泡添加至scrollview上
        chatScrollView.addSubview(otherMessage)
        //获取当前对话泡泡的最大坐标
        currentMaxY = otherMessage.frame.maxY
        
        
        //添加subview，把scroview送至最底层
        backgroundImage.addSubview(typingDoneButton)
        backgroundImage.addSubview(typingField)
        self.view.addSubview(chatScrollView)
        self.view.addSubview(backgroundImage)
        self.view.sendSubviewToBack(chatScrollView)
       
        
        //添加键盘监听
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyBoardWillShow", name: UIKeyboardWillShowNotification, object: nil)
       
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyBoardWillHide", name: UIKeyboardWillHideNotification, object: nil)
    }

    
   
    
    //button点击事件
    func sendMessageButton(sender: UIButton) {
        print("发送 button clicked")
        //如果textfield不为空，发送消息
        if typingField.text != "" {
            
            //生成己方对话泡泡
            let selfmessageWindow = ChatUIView(y:currentMaxY + 10, width: viewWidth, selfMessage: true, message: typingField.text!, userPhoto: nil).setChatView()
            //对话泡泡添加至scrollview
            chatScrollView.addSubview(selfmessageWindow)
            //更新当前泡泡最大坐标
            currentMaxY = selfmessageWindow.frame.maxY
            contentMaxY = currentMaxY + 20
            print(currentMaxY,"currentMaxY")
            //如果最大Y大于当前窗口高度 更新scrollview的contentsize
            if contentMaxY > chatScrollView.frame.height{
                
                chatScrollView.contentSize = CGSize(width: viewWidth, height: contentMaxY)
                //scrollview自动滚至底部
                chatScrollView.scrollRectToVisible(CGRect(x: 0, y: chatScrollView.contentSize.height - chatScrollView.frame.height, width: viewWidth, height: chatScrollView.frame.height), animated: false)
            }
            //如果最大Y小于当前窗口高度，scrollview的contentsize=当前窗口
            else{
                chatScrollView.contentSize = CGSize(width: viewWidth, height: viewHeight)
                //scrollview自动滚至底部
                chatScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: viewWidth, height: chatScrollView.frame.height), animated: false)
            }
            
            //点击button，textfield 清空
            typingField.text = ""
        }
        
       
        
    }
    
    //失去焦点
    func texFieldDidEndOnExit(sender:UITextField){
       resignFirstResponder()
   }
    
    //键盘弹出事件
    func keyBoardWillShow(){
        print("keyboard did show", "scrollview frame",chatScrollView.frame,chatScrollView.contentSize)
        
        //弹出键盘scrollView高度变小
        chatScrollView.frame = CGRect(x: 0, y: 64, width: viewWidth, height: viewHeight - keyBoardHeight)
        
        //scrollview保持显示最底部
        if contentMaxY > chatScrollView.frame.height {
            
            chatScrollView.scrollRectToVisible(CGRect(x: 0, y: chatScrollView.contentSize.height - viewHeight + keyBoardHeight, width: viewWidth, height: viewHeight - keyBoardHeight), animated: false)
        }
        else{
            chatScrollView.scrollRectToVisible(CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight - keyBoardHeight), animated: false)
        }
        
        //backgroundImage的位置上移
        backgroundImage.frame = CGRect(x: 0, y: 518 - keyBoardHeight, width: viewWidth, height: backgroundImage.frame.height)
        
        print("scroll frame",chatScrollView.frame)
        
    }
    
  
    //键盘隐藏事件
    func keyBoardWillHide(){
        print("keyboard did hide, scrollview frame",chatScrollView.frame)
        
        //所有subview的高度恢复
        chatScrollView.frame = CGRect(x: 0, y: 64, width: viewWidth, height: viewHeight)
        chatScrollView.scrollRectToVisible(CGRect(x: 0, y: chatScrollView.contentSize.height - viewHeight, width: viewWidth, height: viewHeight), animated: false)
        backgroundImage.frame = CGRect(x: 0, y: 518, width: viewWidth, height: backgroundImage.frame.height)
        
        print("scroll frame",chatScrollView.frame)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

}
