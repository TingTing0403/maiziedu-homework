//
//  MessageViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/20/15.
//  Copyright © 2015 liu ting. All rights reserved.
//


//暂不支持动态数据
import UIKit

class MessageViewController: UITableViewController {
    //message界面的data source
    var message = ["Gary","May","Amber","Tika","Kitty"]

    override func viewDidLoad() {
        super.viewDidLoad()
        //有两条未读消息
        self.tabBarItem.badgeValue = "2"
    }
    //section 一个
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //行数等于array的count
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return message.count
    }
    //设置cell
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "MessageTable"
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        //cell的标题为联系人名字
        cell.textLabel?.text = message[indexPath.row]
        //cell的图片为联系人
        cell.imageView?.image = UIImage(named: "emptyStar")
        cell.detailTextLabel?.text = "newest message"
        return cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("clicked message row \(indexPath.row)")
        let chatView = self.storyboard?.instantiateViewControllerWithIdentifier("ChatView")
        self.navigationController?.pushViewController(chatView!, animated: true)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

   
}
