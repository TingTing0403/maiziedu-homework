//
//  SignInViewController.swift
//  WeChatDemo
//
//  Created by liu ting on 9/20/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit
//暂不支持注册功能
class SignInViewController: UIViewController {
    //输入邮箱地址
    @IBOutlet weak var email: UITextField!
    
    @IBAction func emailEditEnd(sender: UITextField) {
        resignFirstResponder()
    }
    //输入用户名
    @IBOutlet weak var userName: UITextField!
    
    @IBAction func userNameEditEnd(sender: UITextField) {
        resignFirstResponder()
    }
    //输入密码
    @IBOutlet weak var password: UITextField!
    
    @IBAction func passwordEditEnd(sender: UITextField) {
        resignFirstResponder()
    }
    //确认密码
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBAction func confirmPasswordEditEnd(sender: UITextField) {
        resignFirstResponder()
    }
    

    @IBAction func signInButton(sender: UIButton) {
        //在邮箱地址 用户名 密码 全部输入 且 两次输入密码一致时 提示注册成功，否则提示注册失败
        if email != nil && userName != nil && password != nil && password.text == confirmPassword.text {
            
            let logInView = self.storyboard?.instantiateViewControllerWithIdentifier("LogInView")
            //注册成功提示
            let alertViewSucceed = UIAlertController(title: "注册成功", message: "注册成功，马上去登录吧", preferredStyle: UIAlertControllerStyle.Alert)
            //注册成功action 好，点击转至登录界面
            let alertButtonGo = UIAlertAction(title: "好", style: UIAlertActionStyle.Default, handler:{(action:UIAlertAction)->Void in self.navigationController?.pushViewController(logInView!, animated: true)})
            //注册成功action 暂不，点击返回注册页面
            let alertButtonNo = UIAlertAction(title: "暂不", style: UIAlertActionStyle.Default, handler: nil)
            
            alertViewSucceed.addAction(alertButtonGo)
            alertViewSucceed.addAction(alertButtonNo)
            //提示
            self.presentViewController(alertViewSucceed, animated: true, completion: nil)
            
         
        }
        else {
            //注册失败action OK，点击返回注册界面
            let alertButtonOK = UIAlertAction(title: "OK", style:UIAlertActionStyle.Default, handler: nil)
            //注册失败alert view
            let alertViewFailed = UIAlertController(title:"注册失败", message: "请重新输入邮箱，用户名，密码，并确保两次输入密码一致", preferredStyle: UIAlertControllerStyle.Alert)
            alertViewFailed.addAction(alertButtonOK)
            self.presentViewController(alertViewFailed, animated: true, completion: nil)
        }
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

 

}
