//
//  ViewController.swift
//  UIControlPractice3
//
//  Created by liu ting on 9/18/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func back(sender: UIBarButtonItem) {
        webView1.goBack()
    }
   
    @IBAction func go(sender: UIBarButtonItem) {
        webView1.loadRequest(NSURLRequest(URL: NSURL(string: textField.text!)!))
        
        print("")
    }
    
    @IBAction func reload(sender: UIBarButtonItem) {
        webView1.reload()
    }
    
   
    @IBAction func goBack(sender: UIBarButtonItem) {
        webView1.goBack()
    }
    

    @IBOutlet weak var webView1: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView1.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        webView1.loadRequest(NSURLRequest(URL: NSURL(string: "http://maiziedu.com")!))
    }
    
    func webViewDidStartLoad(webView: UIWebView){
        print("did start load")
    }
    
    func webViewDidFinishLoad(webView: UIWebView){
        print("did finish load")
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

