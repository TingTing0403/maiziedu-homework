//
//  ScrollViewController.swift
//  UIControlPractice3
//
//  Created by liu ting on 9/19/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController, UIScrollViewDelegate {

    
    @IBOutlet weak var pageViewCell: UIPageControl!
    @IBOutlet var scrollView1: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView1.delegate = self
        //scrollView1.backgroundColor = UIColor.redColor()
        scrollView1.contentSize = CGSize(width: 320*4, height: 586)
      //  scrollView1.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        let view1 = addScrollPageFor5s(0)
        let view2 = addScrollPageFor5s(1)
        let view3 = addScrollPageFor5s(2)
        let view4 = addScrollPageFor5s(3)
        view1.backgroundColor = UIColor.redColor()
        view2.backgroundColor = UIColor.yellowColor()
        view3.backgroundColor = UIColor.greenColor()
        view4.backgroundColor = UIColor.blueColor()
        scrollView1.addSubview(view1)
        scrollView1.addSubview(view2)
        scrollView1.addSubview(view3)
        scrollView1.addSubview(view4)
       // scrollView1.contentOffset = CGPoint(x: 10, y: 10)
        
        scrollView1.indicatorStyle = UIScrollViewIndicatorStyle.White
        pageViewCell.addTarget(self, action: "pageValueChanged", forControlEvents: UIControlEvents.ValueChanged)
        // Do any additional setup after loading the view.
    }
    
    
    
    func addScrollPageFor5s (n:Int)->UIView{
        let view = UIView(frame: CGRect(x: n*320, y: 0, width: 320, height: 586))
        return view
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let curPage = Int(scrollView1.contentOffset.x/320)
        pageViewCell.currentPage = curPage
    }
    @IBAction func pageView(sender: UIPageControl) {
        
        
    }
    

    func pageValueChanged() {
        let pageValue = pageViewCell.currentPage
        scrollView1.scrollRectToVisible(CGRect(x: pageValue*320, y: 0, width: 320, height: 586), animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
