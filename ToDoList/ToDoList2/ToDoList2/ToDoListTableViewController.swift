//
//  ToDoListTableViewController.swift
//  ToDoList2
//
//  Created by liu ting on 9/13/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ToDoListTableViewController: UITableViewController {
    
    var toDoLists = [Item]()
    
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        print("TO DO LIST COUNTS \(toDoLists.count)")
        }

    
    // table view has one section
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    //table view rows = count
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoLists.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //table view cell is reused and should be dequeued using a cell identifier
        let cellIdentifier = "ToDoListTitle"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        let item = toDoLists[indexPath.row]
        
        cell.textLabel?.text = item.title
        
        return cell
    }


    

   
}
