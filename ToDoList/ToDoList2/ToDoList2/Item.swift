//
//  Item.swift
//  ToDoList2
//
//  Created by liu ting on 9/13/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import Foundation

class Item {
    
    //property
    var title = ""
    
    
    //initialize
    init(title: String){
        
        self.title = title
    }
    
    
}
