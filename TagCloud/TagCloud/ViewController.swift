//
//  ViewController.swift
//  TagCloud
//
//  Created by liu ting on 10/7/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var containerWidth:CGFloat!
    var containerHeight:CGFloat!
    var cellHeight:CGFloat!
    var cellWidth:CGFloat!
    var numberOfCellsInRow:CGFloat = 5
    var numberOfCellsInColumn:CGFloat = 5
    var cellCenterX = [CGFloat]()
    var cellCenterY = [CGFloat]()
    var cellCenter = [CGPoint]()
    var boomed = false
    var labels = [UILabel]()
   
    var labelTexts = ["路飞","娜美","索隆","山治","乔巴","乌索普","布鲁克","弗兰奇","罗宾","艾斯"]
   
    
    var labelColors = [UIColor.greenColor(), UIColor.orangeColor(),UIColor.purpleColor(), UIColor.blueColor(), UIColor.magentaColor()]
    var Container = UIView()
  
    //MARK:点击按钮执行动画
    @IBAction func buttonClicked(sender: UIButton) {
        
        centerize()
        boomAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //MARK:标签视图
        Container.frame.size = CGSize(width: self.view.frame.width - 40, height: self.view.frame.width - 40)
        Container.center = self.view.center
        self.view.addSubview(Container)
        
        //MARK:设定标签坐标
        getCellCenters()
        //MARK:设定标签文字
        setLabelText(labelTexts)
        //MARK:默认标签先居中后爆炸
        centerize()
        boomAnimation()
        
    }
    
    //MARK：设置标签中心坐标
    func getCellCenters(){
        containerWidth = Container.frame.width
        containerHeight = Container.frame.height
        
        cellHeight = containerHeight / numberOfCellsInColumn
        cellWidth = containerWidth / numberOfCellsInRow
        
        for i in 1...Int(numberOfCellsInRow) {
            cellCenterX.append(cellWidth/2 + (CGFloat(i)-1) * cellWidth)
        }
        for i in 1...Int(numberOfCellsInColumn) {
            cellCenterY.append(cellHeight/2 + (CGFloat(i)-1) * cellHeight)
        }
        if cellCenterX.isEmpty && cellCenterY.isEmpty{
         print("cellCenterX/Y is empty")
        }
            
        else {
            for m in cellCenterX {
                for n in cellCenterY {
                cellCenter.append(CGPoint(x: m, y: n))
            }
        }
        }
        
    }
    
    //MARK:设置标签文字
    func setLabelText(labelTexts:[String]){

        for labelText in labelTexts {
            
            let newLabel = UILabel()
            newLabel.text = labelText
            
            let i = random()%6
            //MARK:设置标签文本字体随机大小
            let fontSize = CGFloat(i) * 3 + 10
            newLabel.font = UIFont.boldSystemFontOfSize(fontSize)
            //MARK:标签自适应文字大小
            newLabel.sizeToFit()
            
            Container.addSubview(newLabel)
            
            labels.append(newLabel)
            
        }
    }
    
    //MARK:爆炸动画
    func boomAnimation(){
        UIView.animateWithDuration(2, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 5, options: UIViewAnimationOptions.CurveEaseIn, animations: {()->Void in
            
            var centerPoints = self.cellCenter
            
             for label in self.labels {
                if centerPoints.isEmpty == false {
                    //MARK: 随机设置标签中心坐标以及标签颜色
                    let i = random() % centerPoints.count
                    let c = random() % self.labelColors.count
                    label.textColor = self.labelColors[c]
                    label.center = centerPoints[i]
                    centerPoints.removeAtIndex(i)
             }
                else {print("centerPoints is empty")}
            }
         
            }, completion: nil)
        
        
    }
    
    //MARK:标签居中
    func centerize(){
        
        for label in labels {
            label.center = CGPoint(x: containerWidth / 2, y: containerHeight / 2)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}

