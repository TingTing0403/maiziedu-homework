//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

//protocol
protocol LockUnlockProtocol{
    func lock() ->String
    func unlock() -> String
}

class House : LockUnlockProtocol {
    func lock() -> String {
        return "Click"
    }
    
    func unlock() -> String {
        return "Clack"
    }
}

class Vehicle: LockUnlockProtocol {
    func lock() -> String {
        return "Beep-Beep"
    }
    
    func unlock() -> String {
        return "Beep"
    }
}

let myHouse = House()
myHouse.lock()
myHouse.unlock()


let myCar = Vehicle()
myCar.lock()
myCar.unlock()

protocol NewLockUnlockProtocol{
    var locked : Bool {get set}
    func lock() -> String
    func unlock() -> String
}

class Safe: NewLockUnlockProtocol {
    var locked : Bool = false
    
    func lock() -> String {
        locked = true
        return "Ding"
    }
    
    func unlock() -> String {
        locked = false
        return "Dong"
    }
}

class Gate: NewLockUnlockProtocol {
    var locked : Bool = false
    
    func lock() -> String {
        locked = true
        return " Clink!"
    }
    func unlock() -> String {
        locked = false
        return " Clonk"
    }
}

let mySafe = Safe()
mySafe.unlock()
mySafe.lock()

let myGate = Gate()
myGate.lock()
myGate.unlock()

protocol AreaComputationProtocol{
    func computeArea() -> Double
}

protocol PerimeterComputaionProtocol{
    func computePerimeter() -> Double
}

struct Rectangle: AreaComputationProtocol, PerimeterComputaionProtocol {
    var width, height : Double
    
    func computeArea() -> Double {
        return width * height
    }
    
    func computePerimeter() -> Double {
        return width * 2 + height * 2
    }
    
}

var square = Rectangle(width: 3, height: 3)
var rectangle = Rectangle(width: 4, height: 6)

square.computeArea()
square.computePerimeter()

rectangle.computePerimeter()
rectangle.computeArea()

protocol TriangleProtocol : AreaComputationProtocol, PerimeterComputaionProtocol{
    var a : Double {get set}
    var b : Double {get set}
    var c : Double {get set}
    var base : Double {get set}
    var height : Double {get set}
}

struct Triangle : TriangleProtocol {
    var a,b,c : Double
    var height, base :Double
    func computeArea()->Double{
        return (base*height)/2
    }
    func computePerimeter()->Double{
        return (a+b+c)
    }
}

var triangle1 = Triangle(a: 2, b: 2, c: 3, height: 2, base: 10)

triangle1.computeArea()
triangle1.computePerimeter()

protocol VendingMachineProtocol{
    var coinInserted : Bool { get set }
    func shouldVend()->Bool
}

class Vendor : VendingMachineProtocol {
    var coinInserted : Bool = false
    func shouldVend() -> Bool {
        if coinInserted == true {
            coinInserted = false
            return true
        }
        return false
    }
}

class ColaMachine {
    var vendor : VendingMachineProtocol
    
    init(vendor : VendingMachineProtocol){
        self.vendor = vendor
    }
    
    func insertCoin (){
        vendor.coinInserted = true
    }
    
    func pressColaButton() -> String{
        if vendor.shouldVend() == true {
        return "Here is a Cola"
        }
        else {
            return "You must insert a coin"
        }
        
    }
    
    func pressRootBeerButton() ->String {
        if vendor.shouldVend() == true {
            return "Here is a Beer"
        }
        else {
            return "please insert a coin"
        }
    }
}

    var vendingMachine = ColaMachine(vendor: Vendor())

    vendingMachine.pressColaButton()
vendingMachine.insertCoin()
vendingMachine.pressColaButton()
vendingMachine.pressRootBeerButton()
vendingMachine.insertCoin()
vendingMachine.pressRootBeerButton()

extension ColaMachine{
    func pressDietColaButton() -> String{
        if vendor.shouldVend() == true {
            return "Here is a Diet Cola"
        }
        else {
            return " please insert a coin"
        }
    }
}

var newVendingMachine = ColaMachine(vendor: Vendor())

vendingMachine.insertCoin()
vendingMachine.pressDietColaButton()


