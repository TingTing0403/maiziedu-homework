//
//  TableViewController.swift
//  tableViewPractice
//
//  Created by liu ting on 9/24/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    
    var province = ["四川","山东","江苏"]
    var city = ["四川":["成都","眉山","资阳","宜昌","德阳","绵阳","内江","达州","宜宾","攀枝花"],"山东":["济南","青岛","潍坊","淄博","威海","东营","济宁","泰安","德州","枣庄"],"江苏":["南京","镇江","连云港","徐州","宿迁","无锡","苏州","常州","盐城","昆山"]]
    
    var insertEnabled = false
    var deleteEnabled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  let refreshControl = UIRefreshControl(frame: CGRect(x: 0, y: 64, width: 400, height: 50))
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "下拉刷新")
        self.refreshControl?.addTarget(self, action: "refreshData", forControlEvents: UIControlEvents.ValueChanged)
        
    }
    
    func refreshData(){
        print("reloadData")
        city["四川"]?.append("广安")
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
        
    }
    
    @IBOutlet weak var editButton: UIBarButtonItem!

    //设置可编辑状态
    @IBAction func editButtonClicked(sender: UIBarButtonItem) {
        deleteEnabled = !deleteEnabled
        insertEnabled = false
        tableView.setEditing(deleteEnabled, animated: true)
        if tableView.editing {
            editButton.title = "Done"
        }
        else {
            editButton.title = "Edit"
        }
       
    }
    
    @IBAction func insertButtonClicked(sender: UIBarButtonItem) {
        insertEnabled = !insertEnabled
        deleteEnabled = false
        tableView.setEditing(insertEnabled, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }

    
    //section 数目
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return province.count
    }
    //行数
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return city[province[section]]!.count
    }
    //section 标题
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return province[section]
    
    }
    
    //reuse identifier 单元格设置
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        cell.textLabel?.text = city[province[indexPath.section]]![indexPath.row]
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        return cell
    }
    //添加索引
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return ["A","B","C"]
    }
    
    //设置列表的可编辑状态
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        
            return true

    }
    //设置editing style
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if deleteEnabled{
           return UITableViewCellEditingStyle.Delete
        }
        else if insertEnabled {
            return UITableViewCellEditingStyle.Insert
        }
        else {
            return UITableViewCellEditingStyle.None
        }
        
    }
    
    // 执行editing
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // 先删除数组元素，再删除tableview中的行。否则会出错
            city[province[indexPath.section]]?.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            
        } else if editingStyle == .Insert {
            city[province[indexPath.section]]?.insert(city[province[indexPath.section]]![indexPath.row], atIndex: indexPath.row)
            tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
            
        }    
    }
    
   


    
    // 移动单元格，先移动数组数据再移动单元格
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        let fromProvince = province[fromIndexPath.section]
        let fromCity = city[fromProvince]![fromIndexPath.row]
        let toProvince = province[toIndexPath.section]
        city[fromProvince]!.removeAtIndex(fromIndexPath.row)
        city[toProvince]?.insert(fromCity, atIndex: toIndexPath.row)
        tableView.moveRowAtIndexPath(fromIndexPath, toIndexPath: toIndexPath)
        
    }
    

    
    // 允许移动单元格
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailTable = self.storyboard?.instantiateViewControllerWithIdentifier("detailTable")
        
        self.navigationController?.pushViewController(detailTable!, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
