//
//  ViewController.swift
//  ToDoList3
//
//  Created by liu ting on 9/13/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var rowIndex = 0
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "RowIndex is \(rowIndex)"
       
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }


}

