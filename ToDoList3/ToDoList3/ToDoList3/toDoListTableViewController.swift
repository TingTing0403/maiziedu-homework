//
//  toDoListTableViewController.swift
//  ToDoList3
//
//  Created by liu ting on 9/13/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class toDoListTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print(indexPath.row)
        
        let selectedViewController = self.storyboard?.instantiateViewControllerWithIdentifier("toDoListDetail") as! ViewController
        selectedViewController.rowIndex = indexPath.row
        
        self.navigationController?.pushViewController(selectedViewController, animated: true)
    
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
