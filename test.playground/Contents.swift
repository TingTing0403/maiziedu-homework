//class Door 类 继承


var str = "hello, playground"

class Portal {
    var opened: Bool = false
    var locked: Bool = false
    let width: Int
    let height: Int
    let weight: Int
    let name: String
    var color: String
    
    init(name : String, width : Int = 32, height : Int = 72, weight : Int = 20, color : String = "Red"){
        self.name = name
        self.width = width
        self.height = height
        self.weight = weight
        self.color = color
    }
    
    func open() ->String{
        opened = true
        return "C-r-r-e-e-a-k-k-k...the \(name) is open!"
    }
    
    func close() ->String{
        opened = false
        return "C-r-r-e-e-a-k-k-k... the \(name) is closed!"
    }
    
    func lock()->String{
        locked = true
        return "C-l-i-c-c-c-k-k...the \(name) is locked!"
    }
    
    func unlock()->String{
        locked = false
        return "C-l-i-c-c-c-k-k... the \(name) is unlocked!"
    }
}



class NewDoor{
    var opened: Bool = false
    var locked: Bool = false
    let width: Int
    let height: Int
    let weight: Int
    var color: String
    
    init(width : Int = 32, height : Int = 72, weight : Int = 20, color : String = "Red"){
        self.width = width
        self.height = height
        self.weight = weight
        self.color = color
    }
    func open() ->String{
        if locked == true {
            return "You need to unlock the door first!"
        }
        else if opened==false{
        opened = true
        return "C-r-r-e-e-a-k-k-k...the door is open!"
        }
        else {
            return "The door is already open"
        }
    }
    func close() ->String{
        if opened == true {
            opened = false
            return "C-r-r-e-e-a-k-k-k... the door is closed!"
        }
        else {
            return "The door is already closed!"
        }
    }
    
    func lock()->String{
        if opened == true {
            return "You can't lock an open door"
        }
        else if locked == false {
            locked = true
            return "C-l-i-c-c-c-k-k...the door is locked!"
        }
        else {
            return "The door is locked!"
        }
    }
    
    func unlock()->String{
        if locked == true {
            locked = false
            return "C-l-i-c-c-c-k-k... the door is unlocked!"

        }
        else {
            return "The door is already unlocked!"
        }
    }
    
}
let newBackDoor = NewDoor()

let newFrontDoor = NewDoor(width: 30, height: 80, weight: 20, color: "Green")

newFrontDoor.open()
newFrontDoor.lock()
newFrontDoor.close()
newFrontDoor.lock()
newFrontDoor.open()

newFrontDoor.color = " Brown "

class NiceDoor: Portal {
    init(width: Int = 32, height: Int = 72 , weight: Int = 20, color: String = "Red") {
        super.init(name: "door", width: width, height: height, weight: weight, color: color)
    }
}

class NewWindow: Portal {
    init(width: Int = 40, height: Int = 60, weight: Int = 22, color: String = "Yellow") {
        super.init(name: "window", width: width, height: height, weight: weight, color: color)
    }
}

let myNiceDoor = NiceDoor(width: 23, height: 75, weight: 30, color: "Black")
let myNewWindow = NewWindow(width: 50, height: 90, weight: 37, color: "Blue")

myNiceDoor.open()
myNiceDoor.lock()
myNewWindow.open()

class CombinationDoor: NiceDoor {
    var combinationCode: String?
    
    override func lock() -> String {
        return "the method is not valid for a combination door!"
    }
    
    override func unlock() -> String {
        return "the method is not valid for a combination door!"
    }
    func lock(combinationCode: String) ->String{
        if opened == false {
            if locked == true {
                return "the \(name) is already locked"
            }
            else {
            self.combinationCode = combinationCode
            locked = true
            return "C-l-i-c-c-c-k-k...the \(name) is locked!"
            }
        }
        else {
            return "you can't lock an open \(name)"
        }
        
    }
    
    func unlock(combinationCode : String) -> String{
        if opened == false {
            if locked == true {
                if self.combinationCode != combinationCode {
                    return "wrong code... the \(name) is still locked"
                }
                else {
                    locked = false
                    return " C-l-i-c-c-c-k-k...the \(name) is unlocked "
                }
            }
            else {
                return " The \(name) is already unlocked!"
                
            }
        }
        else {
            return "you can not unlock an open \(name)"
        }
    }
}

let secureDoor = CombinationDoor(width: 20, height: 50, weight: 20, color: "red")
secureDoor.lock("12345")
secureDoor.unlock("14568")
secureDoor.unlock()
secureDoor.unlock("12345")

enum FuelType : String{
    case Gasoline = "89 octane"
    case Diesel = "sulphur free"
    case Biodiesel = "vegtable oil"
    case Electric = "30 amps"
    case NaturalGas = "coalbed methane"
}

var engine : FuelType = .Gasoline

var vehicleName : String

switch engine {
case .Gasoline:
    vehicleName = " Ford F-150"
 
case .Diesel:
    vehicleName = " Ford F250"
case .Biodiesel:
    vehicleName = "Custom Van"
    
case .Electric:
    vehicleName = " Toyota Prius"
    
case .NaturalGas:
    vehicleName = " Utility Truck"
}

print("Vehicle \(vehicleName) takes \(engine.rawValue)")

enum TransmissionType{
    case Manual4Gear
    case Manual5Gear
    case Automatic
}

struct Vehicle {
    var fuel : FuelType
    var transmission : TransmissionType
}

var dieseAutomatic = Vehicle(fuel: .Diesel, transmission: .Automatic)

struct Structure {
    var copyVar : Int = 10
}

var struct1 = Structure()

var struct2 = struct1
struct2.copyVar = 20
struct1.copyVar

class Class {
    var copyVar : Int = 10
}

var class1 = Class()
var class2 = class1
class2.copyVar = 24
class1.copyVar

var i:Float = 4.9, k: Float = 5
while i < k{
    i = i + 0.1
    print(i)
}

var name = "sundy"
var addname = "AAA" + name

print(addname)

import Foundation



