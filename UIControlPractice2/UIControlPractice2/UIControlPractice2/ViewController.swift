//
//  ViewController.swift
//  UIControlPractice2
//
//  Created by liu ting on 9/14/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DatePickerViewControllerDelegate{
    
    @IBOutlet weak var myLabel: UILabel!
    
    var  datePicker = DatePickerViewController()
    
    
    func changeLabel(newString: String) {
        self.myLabel.text = newString
    }
    @IBOutlet weak var stepperValue: UIStepper!
    
    @IBOutlet weak var textField: UITextField!
    
    //textField 显示stepper的值
    @IBAction func valueChanged(sender: UIStepper) {
        textField.text = NSString(format: "%g", sender.value) as String
    }
   
    @IBAction func stopButtom(sender: UIButton) {
        
        let alertView = UIAlertController(title: "Stop animation", message: "Are you sure to stop animation?", preferredStyle: UIAlertControllerStyle.Alert)
        let alertActionYes = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{(action:UIAlertAction)->Void in
            
            self.imageView.stopAnimating()} )
        let alertActionNo = UIAlertAction(title: "No", style: UIAlertActionStyle.Default, handler:nil)// {(action:UIAlertAction)->Void in self.imageView.startAnimating()})
      
            alertView.addAction(alertActionYes)
            alertView.addAction(alertActionNo)
        alertView.addTextFieldWithConfigurationHandler({(textFieldAlert: UITextField)-> Void in
            
            textFieldAlert.placeholder = "user name"
            alertActionYes.enabled = false
            
            })
        alertView.addTextFieldWithConfigurationHandler({(textFieldAlert: UITextField)-> Void in
            textFieldAlert.placeholder = "password"
            })
    
        
        self.presentViewController(alertView, animated: true, completion: nil)
        
        
    }
//      let datePicker = self.storyboard?.instantiateViewControllerWithIdentifier("datePicker")
//    self.navigationController?.pushViewController(datePicker!, animated: true)
//            print("push")
    @IBAction func actionSheet(sender: UIButton) {
      
        let showActionSheet = UIAlertController(title: " Stop Animation?", message: "Are you sure to stop animation?", preferredStyle: UIAlertControllerStyle.ActionSheet)
        let showActionSheetYes = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler: {(action: UIAlertAction)-> Void in
            self.imageView.stopAnimating()
        })
        
        let showActionSheetNo = UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler: nil)
        showActionSheet.addAction(showActionSheetYes)
        showActionSheet.addAction(showActionSheetNo)
        self.presentViewController(showActionSheet, animated: true, completion: nil)
        
    }
    
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.delegate = self
        // textField设置初始值
        textField.text = NSString(format: "%g", stepperValue.value) as String
    //imageView.image = UIImage(named: "1")
       imageView.animationImages = [UIImage(named: "1")!, UIImage(named: "2")!, UIImage(named: "3")!, UIImage(named: "4")!, UIImage(named: "5")!, UIImage(named: "6")!]
        imageView.animationRepeatCount = 100
        imageView.animationDuration = 1
        imageView.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc2 = segue.destinationViewController as! DatePickerViewController
        vc2.delegate = self
    }


}

