//
//  PickerViewController.swift
//  UIControlPractice2
//
//  Created by liu ting on 9/18/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class PickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var pickerView1: UIPickerView!
    
    var selectedProvince = ""
    
    let province = ["四川","山东","广东","江苏"]
    let city = ["四川":["成都","德阳","宜宾","内江","泸州"], "山东":["济南","青岛","潍坊","威海"], "广东":["广州","珠海","佛山"],"江苏":["南京","无锡"]]

    override func viewDidLoad() {
        super.viewDidLoad()
//        numberOfComponentsInPickerView(pickerView1)
//        pickerView(pickerView1, numberOfRowsInComponent: 2)
        pickerView1.dataSource = self
        pickerView1.delegate = self
        
        // Do any additional setup after loading the view.
    }
    //设定列数
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    //设定每列行数,第一列行数等于省份数，第二列行数等于第一列选中省份的城市数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return province.count
        }else {
            if city[selectedProvince] != nil{
            return (city[selectedProvince]?.count)!
            }
            else {
                return 0
            }
        }
    }
    //设定每行title，第一列title为省份名，第二列title为选中省份之下的城市名
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return province[row]
        }else {
            return city[selectedProvince]![row]
        }
    }
    
    //记录第一列选了哪一行，以便重新加载该省的城市到第二列
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
            selectedProvince = province[row]
            pickerView1.reloadComponent(1)
        }
        pickerView1.selectRow(0, inComponent: 1, animated: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
