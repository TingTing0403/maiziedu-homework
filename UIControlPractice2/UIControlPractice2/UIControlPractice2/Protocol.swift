//
//  Protocol.swift
//  UIControlPractice2
//
//  Created by liu ting on 9/16/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import Foundation

protocol DatePickerViewControllerDelegate {
    func changeLabel (newString: String)
}
