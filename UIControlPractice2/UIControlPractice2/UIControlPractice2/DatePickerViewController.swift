//
//  DatePickerViewController.swift
//  UIControlPractice2
//
//  Created by liu ting on 9/15/15.
//  Copyright © 2015 liu ting. All rights reserved.
//

import UIKit

class DatePickerViewController :UIViewController {
    var delegate : DatePickerViewControllerDelegate?
    
    @IBOutlet weak var textFieldOutlet: UITextField!
    @IBAction func myTextField(sender: UITextField) {
        resignFirstResponder()
        print(textFieldOutlet.text)
    }
    
    @IBAction func chengTextButton(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.delegate?.changeLabel(textFieldOutlet.text!)
    }
    
    
    @IBOutlet weak var countDownTimer: UIDatePicker!
    @IBAction func countDownButton(sender: UIButton) {
        countDownTimer.countDownDuration -= 60
    }
    
//    @IBAction func datePicker(sender: UIDatePicker) {
//        
//        let format = NSDateFormatter()
//        format.dateFormat = "yyyy年MM月dd日hh时mm分ss秒"
//        
//        let dateString = format.stringFromDate(sender.date)
//        print(dateString)
    
//      }

   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
